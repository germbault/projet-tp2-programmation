import { Router } from 'express';
import { activityMap } from '../datamap';
import { ActivityModel } from '../model/activitymodel';
import { wrap } from '../util';
import { participantRouter } from './participantrouter';

let nextActivityId = 1;

const activityRouter = Router();

/**
 * MiddleWare
 */
activityRouter.use('/:activityId', wrap(async (req, res, next) => {
    const activity = activityMap.get(parseInt(req.params.activityId));
    if (activity === undefined) {
        return res.sendStatus(404);
    }
    req.activity = activity;
    return next();
}));

/**
 * Pour retourner tous les items de la list
 */
activityRouter.get('/', wrap(async (_req, res) => {
    const activity = Array.from(activityMap.values());
    return res.send(activity);
}));

/**
 * Pour retourner qu'un seul items de la list avec l'id
 * Code pour soustraire un élément du tableau ActivityModel copier durant la requête HTTP
 */
activityRouter.get('/:activityId', wrap(async (req, res) => {
    const modelToSend = { ...req.activity };
    delete modelToSend.participant;
    return res.send(modelToSend);
}));

/**
 * Pour de créer plusieurs items en donnant un identifiant différent a chaque fois dans PostMan
 * Option pour convertir la date
 */
activityRouter.post('/', wrap(async (req, res) => {
    const activity: ActivityModel = req.body;
    activity.activityId = nextActivityId;
    nextActivityId++;
    activity.startDate = new Date(activity.startDate);
    activity.participant = new Map;
    activityMap.set(activity.activityId, activity);

    return res.send(activity);
}));

/**
 * Pour de modifier un item de la list
 */
activityRouter.put('/:activityId', wrap(async (req, res) => {
    const Updated: ActivityModel = req.body;
    req.activity.name = Updated.name;
    return res.send(req.activity);
}));

/**
 * Pour supprimer un items de la list avec l'id
 */
activityRouter.delete('/:activityId', wrap(async (req, res) => {
    activityMap.delete(req.activity.activityId);
    return res.sendStatus(204);
}));

activityRouter.use('/:activityId/participant', participantRouter);

export { activityRouter };
