import { Router } from 'express';
import { activityMap } from '../datamap';
import { ParticipantModel } from '../model/participantmodel';
import { wrap } from '../util';

const participantRouter = Router();

let nextParticipantId = 1;

/**
 * MiddleWare
 * Utilisation du e
 * https://riptutorial.com/fr/typescript/example/29544/recherche-d-objet-dans-un-tableau
 */
participantRouter.use('/:participantId', wrap(async (req, res, next) => {
    const participantMap = Array.from(req.activity.participant.values());
    const participant = participantMap.find(e => e.participantId === parseInt(req.params.participantId));
    if (participant === undefined) {
        return res.sendStatus(404);
    }
    req.participant = participant;
    return next();
}));

/**
 * Pour retourner tous les items de la list
 * Vérification si l'item exist dans le MiddleWare
 */
participantRouter.get('/', wrap(async (req, res) => {
    const participant = Array.from(req.activity.participant.values());
    return res.send(participant);
}));

/**
 * Pour retourner qu'un seul items de la list avec l'id
 */
participantRouter.get('/:participantId', wrap(async (req, res) => {
    const modelToSend = { ...req.participant };
    return res.send(modelToSend);
}));


/**
 * Pour de créer plusieurs items en donnant un identifiant différent a chaque fois dans PostMan
 */
participantRouter.post('/', wrap(async (req, res) => {
    const participant: ParticipantModel = req.body;
    participant.participantId = nextParticipantId;
    nextParticipantId++;
    req.activity.participant.set(participant.participantId, participant);

    return res.send(participant);
}));

/**
 * Pour modifier un item de la list
 */
participantRouter.put('/:participantId', wrap(async (req, res) => {
    const Updated: ParticipantModel = req.body;
    req.participant.participantName = Updated.participantName;
    return res.send(req.participant);
}));

/**
 * Pour supprimer un items de la list avec l'id
 */
participantRouter.delete('/:participantId', wrap(async (req, res) => {
    activityMap.delete(req.participant.participantId);
    return res.sendStatus(204);
}));

export { participantRouter };
