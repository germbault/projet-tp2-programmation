import { ActivityModel } from './model/activitymodel';

// le map contient l'indentifiant et ActivityModel contient la valeur qui est le name
export const activityMap = new Map<number, ActivityModel>();
