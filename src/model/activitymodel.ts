import { ParticipantModel } from './participantmodel';

export class ActivityModel {
    public activityId: number;
    public name: string;
    public startDate: Date;
    public participant: Map<number, ParticipantModel>;
}
