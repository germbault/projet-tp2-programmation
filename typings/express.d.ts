import { ActivityModel } from '../src/model/activitymodel';
import { ParticipantModel } from '../src/model/participantmodel';

/**
 * Déclare une extension au type d'express (Pour la création du middleware)
 * Sans quoi il sera undifined dans la requete
 */
declare global {
    module Express {
        interface Request {
            activity: ActivityModel;
            participant: ParticipantModel;
        }
    }
}
